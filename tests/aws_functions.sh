#!/bin/bash

# disable some warnings as shellcheck is not able to see the dynamically
# imported functions via eval() and has some assumptions about variable use
# that are not valid for unit tests
# shellcheck disable=SC2034,SC2030,SC2031

set -euo pipefail

source scripts/functions.sh
source tests/helpers.sh

say "aws_functions"

_failed_init

# stub aws command and record params and relevant exported env variables
function aws {
    echo "$@" > /tmp/aws_params
    bash -c 'echo $AWS_ACCESS_KEY_ID' > /tmp/aws_access_key
    bash -c 'echo $AWS_SECRET_ACCESS_KEY' > /tmp/aws_secret_key
}

function _clear_aws {
    rm -f /tmp/aws_params /tmp/aws_access_key /tmp/aws_secret_key
}

function _check_leaked_vars {
    local aws_var_count
    aws_var_count=$(set | grep -c ^AWS_ || true)

    _check_equal "${aws_var_count}" "0" "len(env[AWS_*])" "Did no environment variables leak from the AWS functions"
}

function check_bucket_slash {
    local BUCKET="AWS|bucket/|path/"
    eval "$(aws_vars BUCKET)"

    _check_equal "${AWS_S3_BUCKET}" "bucket" "AWS_S3_BUCKET" "Is a trailing slash at the end of the bucket removed"
}
check_bucket_slash

function check_path_no_slash {
    local BUCKET="AWS|bucket|path"
    eval "$(aws_vars BUCKET)"

    _check_equal "${AWS_S3_PATH}" "path/" "AWS_S3_PATH" "Is a slash added to a path without one"
}
check_path_no_slash

function check_path_empty {
    local BUCKET="AWS|bucket|"
    eval "$(aws_vars BUCKET)"

    _check_equal "${AWS_S3_PATH}" "" "AWS_S3_PATH" "Is no slash added to an empty path"
}
check_path_empty

function check_path_slash {
    local BUCKET="AWS|bucket|path/"
    eval "$(aws_vars BUCKET)"

    _check_equal "${AWS_S3_PATH}" "path/" "AWS_S3_PATH" "Is no slash added to a path that already has one"
}
check_path_slash

function check_aws {
    local AWS_ACCESS_KEY_ID_exists AWS_SECRET_ACCESS_KEY_exists BUCKET="AWS|bucket|path/"
    eval "$(aws_vars BUCKET)"

    AWS_ACCESS_KEY_ID_exists="$([ -v AWS_ACCESS_KEY_ID ] && echo 1 || echo 0)"
    AWS_SECRET_ACCESS_KEY_exists="$([ -v AWS_SECRET_ACCESS_KEY ] && echo 1 || echo 0)"

    _check_equal "${AWS_ACCESS_KEY_ID_exists}" "0" "-v AWS_ACCESS_KEY_ID" "Is the access key kept undefined if requested"
    _check_equal "${AWS_SECRET_ACCESS_KEY_exists}" "0" "-v AWS_SECRET_ACCESS_KEY" "Is the secret key kept undefined if requested"
    _check_equal "${AWS_S3_ADDRESSING_MODEL}" "host" "AWS_S3_ADDRESSING_MODEL" "Is the addressing model 'host' without an explicit endpoint"
    _check_equal "${AWS_S3_ENDPOINT_PARAMS}" "" "AWS_S3_ENDPOINT_PARAMS" "Is the endpoint parameter omitted without an explicit endpoint"
    _check_equal "${AWS_S3_ENDPOINT_HOST}" "s3.amazonaws.com" "AWS_S3_ENDPOINT_HOST" "Does the endpoint host default to AWS"
    _check_equal "${AWS_S3_ENDPOINT_SCHEMA}" "http" "AWS_S3_ENDPOINT_SCHEMA" "Does the endpoint schema default to http"
}
check_aws

function check_local_endpoint {
    local BUCKET="S3|bucket|path/"
    local S3_ENDPOINT="http://localhost:8080"
    local S3_ACCESS_KEY="access-key"
    local S3_SECRET_KEY="secret-key"
    eval "$(aws_vars BUCKET)"

    _check_equal "${AWS_ACCESS_KEY_ID}" "access-key" "AWS_ACCESS_KEY_ID" "Is the access key correct"
    _check_equal "${AWS_SECRET_ACCESS_KEY}" "secret-key" "AWS_SECRET_ACCESS_KEY" "Is the secret key correct"
    _check_equal "${AWS_S3_ADDRESSING_MODEL}" "path" "AWS_S3_ADDRESSING_MODEL" "Is the addressing model 'path' with an explicit endpoint"
    _check_equal "${AWS_S3_ENDPOINT_PARAMS}" "--endpoint-url http://localhost:8080" "AWS_S3_ENDPOINT_PARAMS" "Is the endpoint parameter specified with an explicit endpoint"
    _check_equal "${AWS_S3_ENDPOINT_HOST}" "localhost:8080" "AWS_S3_ENDPOINT_HOST" "Is the endpoint host correct"
    _check_equal "${AWS_S3_ENDPOINT_SCHEMA}" "http" "AWS_S3_ENDPOINT_SCHEMA" "Is the endpoint schema correct"
}
check_local_endpoint

function check_overridden_addressing_model {
    local BUCKET="S3|bucket|path/"
    local S3_ENDPOINT="http://s3-storage.example.com"
    local S3_ADDRESSING_MODEL="host"
    eval "$(aws_vars BUCKET)"

    _check_equal "${AWS_S3_ADDRESSING_MODEL}" "host" "AWS_S3_ADDRESSING_MODEL" "Can the addressing model be overridden"
}
check_overridden_addressing_model

function check_endpoint_trailing_slash {
    local BUCKET="S3|bucket|path/"
    local S3_ENDPOINT="http://localhost:8080/"
    eval "$(aws_vars BUCKET)"

    _check_equal "${AWS_S3_ENDPOINT_HOST}" "localhost:8080" "AWS_S3_ENDPOINT_HOST" "Is a trailing slash removed from endpoint host"
}
check_endpoint_trailing_slash

function check_aws_s3_download_command {
    local BUCKET="AWS|bucket/|path"
    _clear_aws
    aws_s3_download BUCKET from to

    _check_leaked_vars
    _check_equal "$(cat /tmp/aws_access_key)" "" "aws_access_key" "Is the download access key correct for AWS"
    _check_equal "$(cat /tmp/aws_secret_key)" "" "aws_secret_key" "Is the download secret key correct for AWS"
    _check_equal "$(cat /tmp/aws_params)" "s3 cp s3://bucket/path/from to" "aws_params" "Is the download command correct for AWS"
}
check_aws_s3_download_command

function check_aws_s3_download_dir_command {
    local BUCKET="AWS|bucket/|path"
    _clear_aws
    aws_s3_download_dir BUCKET from to

    _check_leaked_vars
    _check_equal "$(cat /tmp/aws_access_key)" "" "aws_access_key" "Is the sync access key correct for AWS"
    _check_equal "$(cat /tmp/aws_secret_key)" "" "aws_secret_key" "Is the sync secret key correct for AWS"
    _check_equal "$(cat /tmp/aws_params)" "s3 sync s3://bucket/path/from to --size-only" "aws_params" "Is the sync command correct for AWS"
}
check_aws_s3_download_dir_command

# function() instead of function{} so that the stub functions are scoped
function check_aws_s3_download_artifacts_command() (
    local BUCKET_ARTIFACTS="AWS|bucket/|path"
    local CI_PROJECT_DIR="/tmp"
    local CI_PIPELINE_ID="12345"
    local ARTIFACT_DEPENDENCY="job"
    local PREVIOUS_JOB_ID="67890"
    local ARTIFACTS_DIR="/tmp/artifacts"
    local ARTIFACTS_TEMP_SUFFIX="-temp"
    local RC_FILE="/tmp/rc"
    local artifacts_mode="s3"
    local MAX_TRIES=20
    local MAX_WAIT=5

    function aws_s3api_list_objects {
        if [ "$2" = "${CI_PIPELINE_ID}/${ARTIFACT_DEPENDENCY}/" ]; then
            echo "path/${CI_PIPELINE_ID}/${ARTIFACT_DEPENDENCY}/${PREVIOUS_JOB_ID}/"
        else
            echo "path/${CI_PIPELINE_ID}/${ARTIFACT_DEPENDENCY}/${PREVIOUS_JOB_ID}/rc"
            echo "path/${CI_PIPELINE_ID}/${ARTIFACT_DEPENDENCY}/${PREVIOUS_JOB_ID}/artifacts/"
        fi
    }
    _clear_aws
    echo dummy > "${RC_FILE}"
    echo dummy > "${RC_FILE}${ARTIFACTS_TEMP_SUFFIX}"
    aws_s3_download_artifacts

    _check_leaked_vars
    _check_equal "$(cat /tmp/aws_access_key)" "" "aws_access_key" "Is the sync access key correct for AWS"
    _check_equal "$(cat /tmp/aws_secret_key)" "" "aws_secret_key" "Is the sync secret key correct for AWS"
    _check_equal "$(cat /tmp/aws_params)" "s3 sync s3://bucket/path/12345/job/67890/artifacts /tmp/artifacts-temp --size-only" "aws_params" "Is the sync command correct for AWS"
)
check_aws_s3_download_artifacts_command

function check_aws_s3_upload_command {
    local BUCKET="AWS|bucket/|path"
    _clear_aws
    aws_s3_upload BUCKET from to

    _check_leaked_vars
    _check_equal "$(cat /tmp/aws_access_key)" "" "aws_access_key" "Is the upload access key correct for AWS"
    _check_equal "$(cat /tmp/aws_secret_key)" "" "aws_secret_key" "Is the upload secret key correct for AWS"
    _check_equal "$(cat /tmp/aws_params)" "s3 cp from s3://bucket/path/to" "aws_params" "Is the upload command correct for AWS"
}
check_aws_s3_upload_command

function check_aws_s3_upload_dir_command {
    local BUCKET="AWS|bucket/|path"
    _clear_aws
    aws_s3_upload_dir BUCKET from to

    _check_leaked_vars
    _check_equal "$(cat /tmp/aws_access_key)" "" "aws_access_key" "Is the sync access key correct for AWS"
    _check_equal "$(cat /tmp/aws_secret_key)" "" "aws_secret_key" "Is the sync secret key correct for AWS"
    _check_equal "$(cat /tmp/aws_params)" "s3 sync from s3://bucket/path/to --size-only" "aws_params" "Is the sync command correct for AWS"
}
check_aws_s3_upload_dir_command

function check_aws_s3_upload_artifacts_command {
    local BUCKET_ARTIFACTS="AWS|bucket/|path"
    local CI_PROJECT_DIR="/tmp"
    local CI_PIPELINE_ID="12345"
    local CI_JOB_NAME="job"
    local CI_JOB_ID="67890"
    local ARTIFACTS_DIR="/tmp/artifacts"
    local RC_FILE="/tmp/rc"
    local artifacts_mode="s3"
    local MAX_TRIES=20
    local MAX_WAIT=5
    _clear_aws
    aws_s3_upload_artifacts

    _check_leaked_vars
    _check_equal "$(cat /tmp/aws_access_key)" "" "aws_access_key" "Is the sync access key correct for AWS"
    _check_equal "$(cat /tmp/aws_secret_key)" "" "aws_secret_key" "Is the sync secret key correct for AWS"
    _check_equal "$(cat /tmp/aws_params)" "s3 sync /tmp/artifacts s3://bucket/path/12345/job/67890/artifacts --size-only" "aws_params" "Is the sync command correct for AWS"
}
check_aws_s3_upload_artifacts_command

function check_aws_s3_ls_command {
    local BUCKET="AWS|bucket/|path"
    _clear_aws
    aws_s3_ls BUCKET dir

    _check_leaked_vars
    _check_equal "$(cat /tmp/aws_access_key)" "" "aws_access_key" "Is the ls access key correct for AWS"
    _check_equal "$(cat /tmp/aws_secret_key)" "" "aws_secret_key" "Is the ls secret key correct for AWS"
    _check_equal "$(cat /tmp/aws_params)" "s3 ls s3://bucket/path/dir" "aws_params" "Is the ls command correct for AWS"
}
check_aws_s3_ls_command

function check_aws_s3api_list_objects_command {
    local BUCKET="AWS|bucket/|path"
    _clear_aws
    aws_s3api_list_objects BUCKET dir param1 param2

    _check_leaked_vars
    _check_equal "$(cat /tmp/aws_access_key)" "" "aws_access_key" "Is the ls access key correct for AWS"
    _check_equal "$(cat /tmp/aws_secret_key)" "" "aws_secret_key" "Is the ls secret key correct for AWS"
    _check_equal "$(cat /tmp/aws_params)" "s3api list-objects --bucket bucket --prefix path/dir param1 param2" "aws_params" "Is the list-objects command correct for AWS"
}
check_aws_s3api_list_objects_command

function check_aws_s3_rm_command {
    local BUCKET="AWS|bucket/|path"
    _clear_aws
    aws_s3_rm BUCKET dir --recursive

    _check_leaked_vars
    _check_equal "$(cat /tmp/aws_access_key)" "" "aws_access_key" "Is the ls access key correct for AWS"
    _check_equal "$(cat /tmp/aws_secret_key)" "" "aws_secret_key" "Is the ls secret key correct for AWS"
    _check_equal "$(cat /tmp/aws_params)" "s3 rm s3://bucket/path/dir --recursive" "aws_params" "Is the rm command correct for AWS"
}
check_aws_s3_rm_command

function check_aws_s3_url_command {
    local BUCKET="AWS|bucket/|path"
    aws_url="$(aws_s3_url BUCKET dir)"

    _check_leaked_vars
    _check_equal "${aws_url}" "http://bucket.s3.amazonaws.com/path/dir" "aws_url" "Is the url correct for AWS"
}
check_aws_s3_url_command

function check_custom_endpoint_s3_download_command {
    local BUCKET="S3|bucket/|path"
    local S3_ENDPOINT="http://localhost:8080/"
    local S3_ACCESS_KEY="access-key"
    local S3_SECRET_KEY="secret-key"
    _clear_aws
    aws_s3_download BUCKET from to

    _check_leaked_vars
    _check_equal "$(cat /tmp/aws_access_key)" "access-key" "aws_access_key" "Is the download access key correct for AWS"
    _check_equal "$(cat /tmp/aws_secret_key)" "secret-key" "aws_secret_key" "Is the download secret key correct for AWS"
    _check_equal "$(cat /tmp/aws_params)" "--endpoint-url http://localhost:8080 s3 cp s3://bucket/path/from to" "aws_params" "Is the download command correct for AWS"
}
check_custom_endpoint_s3_download_command

function check_custom_endpoint_s3_download_dir_command {
    local BUCKET="S3|bucket/|path"
    local S3_ENDPOINT="http://localhost:8080/"
    local S3_ACCESS_KEY="access-key"
    local S3_SECRET_KEY="secret-key"
    _clear_aws
    aws_s3_download_dir BUCKET from to

    _check_leaked_vars
    _check_equal "$(cat /tmp/aws_access_key)" "access-key" "aws_access_key" "Is the upload access key correct for AWS"
    _check_equal "$(cat /tmp/aws_secret_key)" "secret-key" "aws_secret_key" "Is the upload secret key correct for AWS"
    _check_equal "$(cat /tmp/aws_params)" "--endpoint-url http://localhost:8080 s3 sync s3://bucket/path/from to --size-only" "aws_params" "Is the sync command correct for AWS"
}
check_custom_endpoint_s3_download_dir_command

function check_custom_endpoint_s3_upload_command {
    local BUCKET="S3|bucket/|path"
    local S3_ENDPOINT="http://localhost:8080/"
    local S3_ACCESS_KEY="access-key"
    local S3_SECRET_KEY="secret-key"
    _clear_aws
    aws_s3_upload BUCKET from to

    _check_leaked_vars
    _check_equal "$(cat /tmp/aws_access_key)" "access-key" "aws_access_key" "Is the upload access key correct for AWS"
    _check_equal "$(cat /tmp/aws_secret_key)" "secret-key" "aws_secret_key" "Is the upload secret key correct for AWS"
    _check_equal "$(cat /tmp/aws_params)" "--endpoint-url http://localhost:8080 s3 cp from s3://bucket/path/to" "aws_params" "Is the upload command correct for AWS"
}
check_custom_endpoint_s3_upload_command

function check_custom_endpoint_s3_upload_dir_command {
    local BUCKET="S3|bucket/|path"
    local S3_ENDPOINT="http://localhost:8080/"
    local S3_ACCESS_KEY="access-key"
    local S3_SECRET_KEY="secret-key"
    _clear_aws
    aws_s3_upload_dir BUCKET from to

    _check_equal "$(cat /tmp/aws_access_key)" "access-key" "aws_access_key" "Is the upload access key correct for AWS"
    _check_equal "$(cat /tmp/aws_secret_key)" "secret-key" "aws_secret_key" "Is the upload secret key correct for AWS"
    _check_equal "$(cat /tmp/aws_params)" "--endpoint-url http://localhost:8080 s3 sync from s3://bucket/path/to --size-only" "aws_params" "Is the sync command correct for AWS"
}
check_custom_endpoint_s3_upload_dir_command

function check_custom_endpoint_s3_ls_command {
    local BUCKET="S3|bucket/|path"
    local S3_ENDPOINT="http://localhost:8080/"
    local S3_ACCESS_KEY="access-key"
    local S3_SECRET_KEY="secret-key"
    _clear_aws
    aws_s3_ls BUCKET dir

    _check_leaked_vars
    _check_equal "$(cat /tmp/aws_access_key)" "access-key" "aws_access_key" "Is the ls access key correct for AWS"
    _check_equal "$(cat /tmp/aws_secret_key)" "secret-key" "aws_secret_key" "Is the ls secret key correct for AWS"
    _check_equal "$(cat /tmp/aws_params)" "--endpoint-url http://localhost:8080 s3 ls s3://bucket/path/dir" "aws_params" "Is the ls command correct for AWS"
}
check_custom_endpoint_s3_ls_command

function check_custom_endpoint_s3api_list_objects_command {
    local BUCKET="S3|bucket/|path"
    local S3_ENDPOINT="http://localhost:8080/"
    local S3_ACCESS_KEY="access-key"
    local S3_SECRET_KEY="secret-key"
    _clear_aws
    aws_s3api_list_objects BUCKET dir param1 param2

    _check_leaked_vars
    _check_equal "$(cat /tmp/aws_access_key)" "access-key" "aws_access_key" "Is the ls access key correct for AWS"
    _check_equal "$(cat /tmp/aws_secret_key)" "secret-key" "aws_secret_key" "Is the ls secret key correct for AWS"
    _check_equal "$(cat /tmp/aws_params)" "--endpoint-url http://localhost:8080 s3api list-objects --bucket bucket --prefix path/dir param1 param2" "aws_params" "Is the list-objects command correct for AWS"
}
check_custom_endpoint_s3api_list_objects_command

function check_custom_endpoint_s3_url_command {
    local BUCKET="S3|bucket/|path"
    local S3_ENDPOINT="http://localhost:8080/"
    local S3_ACCESS_KEY="access-key"
    local S3_SECRET_KEY="secret-key"
    aws_url="$(aws_s3_url BUCKET dir)"

    _check_leaked_vars
    _check_equal "${aws_url}" "http://localhost:8080/bucket/path/dir" "aws_url" "Is the url correct for AWS"
}
check_custom_endpoint_s3_url_command

_failed_check
