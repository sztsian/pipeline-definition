#!/bin/bash

# disable some warnings as shellcheck is not able to see the dynamically
# imported functions via eval() and has some assumptions about variable use
# that are not valid for unit tests
# shellcheck disable=SC2034,SC2030,SC2031

set -euo pipefail

source scripts/functions.sh
source tests/helpers.sh

say "kcidb_functions"

_failed_init

function check_dump_kcidb_data_on_condition_dont_dump() {
    local TEST_OUTPUT_FILE=/tmp/testfile

    local ARG_PY3=echo
    local ARG_PIPELINE_DEFINITION_DIR=path
    local ARG_RC_FILE=rc
    local ARG_STAGE_NAME=build

    dump_kcidb_data_on_condition "${ARG_PY3}" "${ARG_PIPELINE_DEFINITION_DIR}" "${ARG_RC_FILE}" "${ARG_STAGE_NAME}" build pass fail > "${TEST_OUTPUT_FILE}" || true

    _check_equal "$(cat ${TEST_OUTPUT_FILE})" "Stage status is ${ARG_PIPELINE_DEFINITION_DIR}/scripts/rc_edit.py ${ARG_RC_FILE} rc_state_get stage_${ARG_STAGE_NAME}. Data will be either dumped in later stage, or not at all." "dump_kcidb_data_on_condition status" "Check that adapter is NOT called when passed status doesn't match internal stage_status."
}
check_dump_kcidb_data_on_condition_dont_dump

function check_dump_kcidb_data_on_condition_do_dump() {
    local TEST_OUTPUT_FILE=/tmp/testfile

    local ARG_PY3=echo
    local ARG_PIPELINE_DEFINITION_DIR=path
    local ARG_RC_FILE=rc
    local ARG_STAGE_NAME=build
    local ARG_TO_ADAPTER=build
    local status="${ARG_PIPELINE_DEFINITION_DIR}/scripts/rc_edit.py ${ARG_RC_FILE} rc_state_get stage_${ARG_STAGE_NAME}"

    # Avoid issue created by "source .variables" used internally in the function.
    touch .variables

    dump_kcidb_data_on_condition "${ARG_PY3}" "${ARG_PIPELINE_DEFINITION_DIR}" "${ARG_RC_FILE}" "${ARG_STAGE_NAME}" "${ARG_TO_ADAPTER}" "${status}" > "${TEST_OUTPUT_FILE}" || true

    # Check the last line only.
    _check_equal "$(tail -1 ${TEST_OUTPUT_FILE})" "-m cki.kcidb.adapter ${ARG_TO_ADAPTER}" "dump_kcidb_data_on_condition status" "Check that adapter is called when passed status matches internal stage_status"
}
check_dump_kcidb_data_on_condition_do_dump

_failed_check
