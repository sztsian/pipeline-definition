#!/bin/bash
# All functions needed for setting up a container used in the pipeline.
set -euo pipefail

# Ensure home directory is set.
if [ -z "${HOME:-}" ]; then
    export HOME=/tmp
fi
echo "🏠 Home directory is set: ${HOME}"

# Use the shared functions in this script.
# shellcheck source=scripts/functions.sh
source "${PIPELINE_DEFINITION_DIR}/scripts/functions.sh"

echo_yellow "📦 Container image:"
echo "  GitLab: ${CI_JOB_IMAGE:-}"
if [ -r /etc/cki-image ]; then
    echo "  CKI container image: $(cat /etc/cki-image)"
fi

if [ -v CI_PIPELINE_ID ]; then
    echo_yellow "🏗️ API URLs:"
    echo "  Pipeline: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/pipelines/${CI_PIPELINE_ID}"
    echo "  Variables: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/pipelines/${CI_PIPELINE_ID}/variables"
    echo "  Job: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/jobs/${CI_JOB_ID}"
fi

# Configure curl
cat >~/.curlrc <<EOF
    --location
    --silent
    --show-error
    --retry $MAX_TRIES
    --retry-delay $MAX_WAIT
    --connect-timeout 30
EOF

# Configure retries for wget
cat >~/.wgetrc <<EOF
     tries = $MAX_TRIES
     # maximum wait time used for linear backoff
     waitretry = $MAX_WAIT
EOF

# Ensure the workdir and artifacts directory exist
mkdir -p "${WORKDIR}" "${ARTIFACTS_DIR}"

# Ensure all of the required software is packaged.
# shellcheck source=scripts/prepare_software.sh
source "${PIPELINE_DEFINITION_DIR}/scripts/prepare_software.sh"

# Get CPU count related variables for compiling kernels, running xz etc.
echo_yellow "💻 Determining CPU and job count"
eval "$(get_cpu_count)"
echo "  CPUs: ${CPUS_AVAILABLE}"
echo "  job count: ${MAKE_JOBS}"

# Retrieve artifacts from S3
aws_s3_download_artifacts
