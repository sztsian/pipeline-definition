#!/bin/bash
# Prepare and pre-package all of the software needed in the CKI pipeline to
# avoid extra work and network calls later.
set -euo pipefail

# Enable debug output if DEBUG is set to 1
if [[ ${DEBUG:-} == 1 ]]; then
  set -x
fi

# Use the shared functions in this script.
# shellcheck source=scripts/functions.sh
# shellcheck disable=SC2086 # FIXME warning disabled to enable linting: note: Double quote to prevent globbing and word splitting. [SC2086]
source ${PIPELINE_DEFINITION_DIR}/scripts/functions.sh

# Set the expected name for the software tarball in object storage.
SOFTWARE_OBJECT="pipeline-${CI_PIPELINE_ID}-${SOFTWARE_OBJECT_SUFFIX}.tar.xz"

# Download the prepared software object and untar it.
function get_software_object {
    aws_s3_download BUCKET_SOFTWARE "$SOFTWARE_OBJECT" - | xz -d - | \
        tar --no-same-owner --no-same-permissions --no-xattrs --touch -xf -
}

# Tar the prepared software and upload it.
function upload_software_object {
    tar -cf - software/ | xz -z - | \
        aws_s3_upload BUCKET_SOFTWARE - "${SOFTWARE_OBJECT}"
}

# If the software has already been prepared, download it instead of preparing
# it again.
if is_true "${FORCE_SOFTWARE_PREPARE}"; then
    echo_green "🔄 Force the repackaging of the software."
    rm -rf software
elif [[ -d "${SOFTWARE_DIR}" ]]; then
    echo_green "✅ Prepared software is already downloaded."
    return
else
    aws_s3_ls BUCKET_SOFTWARE "${SOFTWARE_OBJECT}" && s=0 || s=$?
    if [ $s -eq 0 ]; then
        echo_green "📥 Downloading prepared CKI software."
        for _ in {1..5}; do
            get_software_object && s=0 || s=$?
            if [ $s -eq 0 ]; then
                return
            else
                # Delete a partial restore if there was a network problem during
                # download.
                rm -rf software
            fi
        done
    fi
fi

echo_green "📦 Packaging CKI pipeline software."

# Create all of the required directories.
mkdir -p "${SOFTWARE_DIR}"

# Create Python3 venv
loop virtualenv --python=/usr/bin/python3 "${VENV_DIR}"

# Download python packages
for PROJECT in $GITLAB_COM_PACKAGES; do
    loop "${VENV_PY3}" -m pip install "git+https://gitlab.com/cki-project/${PROJECT}.git/"
done

# Download data projects
for PROJECT in $GITLAB_COM_DATA_PROJECTS; do
    git_clone_gitlab_com "${PROJECT}" "${SOFTWARE_DIR}/${PROJECT}" --depth 1
done

# CI systems for projects used in the pipeline can pass in their own versions
# of software that they want to test.
#
# For each of the projects in GITLAB_COM_PACKAGES and GITLAB_COM_DATA_PROJECTS,
# a variable of the following form can be used:
#
#  <project>_pip_url = https://url.com/user/project@BRANCH
#
# For the variable name, dashes in the project name are replaced by underscores.
#
# For the pipeline definition, the following variables are supported:
#  pipeline_definition_repository_override: repo URL
#  pipeline_definition_branch_override: branch name
#
# If this variable is present for a Python package, the currently installed
# version is uninstalled and the new version is installed instead.
#
# If this variable is present for a data project, the currently installed
# version is deleted and the new version is checked out instead.

# Override Python packages if necessary.
for PROJECT in $GITLAB_COM_PACKAGES; do
    override package "$PROJECT"
done

# Override data projects if necessary.
for PROJECT in $GITLAB_COM_DATA_PROJECTS; do
    override data "$PROJECT"
done

# pipeline-definition is special
function download_pipeline_definition() {
    pipeline_definition_url=${pipeline_definition_repository_override:-$PIPELINE_DEFINITION_URL}
    git_clone "${pipeline_definition_url}" "${SOFTWARE_DIR}/pipeline-definition" --depth 1 --branch "${pipeline_definition_branch_override:-main}"
}
if [[ -n "${pipeline_definition_repository_override:-}" ]]; then
    echo_yellow "Found pipeline-definition override for repository: ${pipeline_definition_repository_override}"
fi
if [[ -n "${pipeline_definition_branch_override:-}" ]]; then
    echo_yellow "Found pipeline-definition override for branch: ${pipeline_definition_branch_override}"
fi
download_pipeline_definition

# Upload the compressed archive to object storage without touching the disk.
echo_green "Uploading packaged software to object storage."
loop upload_software_object
